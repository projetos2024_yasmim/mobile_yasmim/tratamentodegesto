import React, { useRef, useEffect, useState } from "react";
import { View, Dimensions, PanResponder, Image, StyleSheet } from "react-native";

const ImageMoviment = () => {
    const [position, setPosition] = useState(0); // posição inicial
    const images = [
        require('../img/agua.jpg'),
        require('../img/onça.jpg'),
        require('../img/paisagem.jpg'),
    ];

    const screenWidth = Dimensions.get("window").width;
    const gestureThreshold = screenWidth * 0.5;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => {
                // Implemente a lógica de movimento aqui se necessário
            },
            onPanResponderRelease: (event, gestureState) => {

                if (gestureState.dx < -gestureThreshold) {

                    setPosition((prevCount) => (prevCount + 1) % images.length);

                } else if (gestureState.dx > gestureThreshold) {
                    setPosition((prevCount) => {

                        if (prevCount === 0) {
                          return images.length - 1;
                        }
            
                        return prevCount - 1;

                    });
                }
            }
        })
    ).current;

    return (
        <View style={styles.container} {...panResponder.panHandlers}>
      {images[position] && ( <Image source={images[position]} style={styles.image} /> )}
    </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#fff'
    },
    image: {
        width: Dimensions.get("window").width / 3,
        height: Dimensions.get("window").width / 3,
        position: 'absolute'
    }
});

export default ImageMoviment;
